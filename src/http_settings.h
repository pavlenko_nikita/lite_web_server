#pragma once
#include <Windows.h>
#include <map>
#include <string>
#include <tchar.h>

namespace LWS
{
    class HttpSettings
    {
    public:
        static HttpSettings& instance();

        void setPort( const std::string& port );
        const std::string& getPortS() const;
        
        void setIp( const std::string& ip );
        const std::string& getIp() const;

        void setDocumentRoot( const std::string& directoryRoot );
        const std::string& getDocumentRoot() const;

        void addDataType( const std::string& type, const std::string& extension );
        bool isDataTypeExist( const std::string& type, const std::string& extension );

        size_t dataTypeCount() const;
        const std::map<std::string, std::string>& getMultiMapDataType() const;

        bool readConfigFile( const std::string& fileName );

    private:
        static const UINT16 s_sizeStrMax;
        std::map<std::string, std::string> m_mmDataTypes;
        std::string m_ip;
        std::string m_documentRoot;
        std::string m_port;

        HttpSettings() = default;
        HttpSettings( const HttpSettings& root ) = delete;
        HttpSettings& operator=( const HttpSettings& ) = delete;

        std::string readConfigParam( const std::string& sectionName, const std::string& paramName, const std::string& fileName );
    };
}

