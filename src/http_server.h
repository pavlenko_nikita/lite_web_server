#pragma once
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <thread>
#include <string>

namespace LWS
{
    class HttpServer
    {
    public:
        ~HttpServer();

        bool start();
        void stop();
        void joinLoop();

    private:
        enum class EServerStatus : UINT8
        {
            eES_Stop = 0,
            eES_Running
        };

        SOCKET m_socketServer = INVALID_SOCKET;
        WSAData m_wsaData = { 0 };
        ADDRINFOA* m_addr = {};
        std::thread m_threadServer;
        TP_CALLBACK_ENVIRON m_callBackEnviron;
        PTP_POOL m_pool = NULL;
        PTP_WORK m_work = NULL;
        PTP_CLEANUP_GROUP m_cleanupGroup = NULL;
        EServerStatus m_statusServer = EServerStatus::eES_Stop;

        void runLoop();
        bool initThreadPool();
        void destroyThreadPool();
    };
}
