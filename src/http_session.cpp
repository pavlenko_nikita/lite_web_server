﻿#include "http_session.h"

#include <sstream>
#include <fstream>
#include <io.h>

const UINT16 LWS::HttpSession::s_sizeBuffer = 128;

LWS::HttpSession::~HttpSession()
{
    closesocket( m_socket );
}

void LWS::HttpSession::start( PTP_CALLBACK_INSTANCE instance, PVOID parameter, PTP_WORK work )
{
    HttpSession workCustom;
    workCustom.setWorkData( instance, parameter, work );
    workCustom.run();
}

void LWS::HttpSession::setWorkData( PTP_CALLBACK_INSTANCE instance, PVOID parameter, PTP_WORK work )
{
    m_socket = reinterpret_cast<SOCKET>(parameter);
}

void LWS::HttpSession::run()
{
    int result = 0;
    std::string strRequest;

    result = readRequest( strRequest );

    if( result > 0 )
    {
        std::stringstream streamResponse;
        std::stringstream streamBody;
 
        size_t pos = strRequest.find_first_of( _T( ' ' ), 0 );
        std::string strRequestType = strRequest.substr( 0, pos++ );
       
        if( strRequestType != "GET" )
        {
            if( !writeFileInBody( HttpSettings::instance().getDocumentRoot() + "/../400.html", streamBody ) )
                return;

            streamResponse << "HTTP/1.1 400 Bad Request\r\n"
                << "Version: HTTP/1.1\r\n"
                << "Content-Type: text/html; charset=utf-8\r\n"
                << "Content-Length: " << streamBody.str().length()
                << "\r\n\r\n"
                << streamBody.str();

            result = writeResponse( streamResponse.str() );
            return;
        }

        std::string strTypeFile = "";
        std::string strResource = HttpSettings::instance().getDocumentRoot() + strRequest.substr( pos, strRequest.find_first_of( _T( ' ' ), pos ) - pos );

        if( !checkResourceDataType( strResource, strTypeFile ) )
        {
            if( !writeFileInBody( HttpSettings::instance().getDocumentRoot() + "/../403.html", streamBody ) )
                return;

            streamResponse << "HTTP/1.1 403 Forbidden\r\n"
                << "Version: HTTP/1.1\r\n"
                << "Content-Type: text/html; charset=utf-8\r\n"
                << "Content-Length: " << streamBody.str().length()
                << "\r\n\r\n"
                << streamBody.str();
        }
        else if( !isResourceExist( strResource ) )
        {
            if( !writeFileInBody( HttpSettings::instance().getDocumentRoot() + "/../404.html", streamBody ) )
                return;

            streamResponse << "HTTP/1.1 404 Not Found\r\n"
                << "Version: HTTP/1.1\r\n"
                << "Content-Type: text/html; charset=utf-8\r\n"
                << "Content-Length: " << streamBody.str().length()
                << "\r\n\r\n"
                << streamBody.str();
        }
        else
        {
            if( !writeFileInBody( strResource, streamBody ) )
                return;
 
            streamResponse << "HTTP/1.1 200 OK\r\n"
                << "Version: HTTP/1.1\r\n"
                << "Content-Type: " << strTypeFile << "; charset=utf-8\r\n"
                << "Content-Length: " << streamBody.str().length()
                << "\r\n\r\n"
                << streamBody.str();
        }
        result = writeResponse( streamResponse.str() );
    }
}

int LWS::HttpSession::readRequest( std::string& strRequest )
{
    CHAR buffer[s_sizeBuffer + 1] = {};
    int result = 0;
    std::stringstream strStreamRequest;
    do
    {
        result = recv( m_socket, buffer, s_sizeBuffer, 0 );
        if( result > 0 )
        {
            buffer[result] = '\0';
            strStreamRequest << buffer;
            if( s_sizeBuffer > result )
            {
                strRequest = strStreamRequest.str();
                break;
            }
        }

    } while( result > 0 );

    return result;
}

int LWS::HttpSession::writeResponse( const std::string& strResponse )
{
    int result = 0;
    do
    {
        result = send( m_socket, &strResponse[result], strResponse.size() - result, 0 );
        if( result == strResponse.length() )
        {
            break;
        }
    } while( result > 0 );
    return result;
}

bool LWS::HttpSession::checkResourceDataType( const std::string& strResource, std::string& strTypeFile )
{
    auto mapDataType = HttpSettings::instance().getMultiMapDataType();   
    size_t pos = std::string::npos;

    for( auto& item : mapDataType )
    {
        pos = strResource.find( item.first );
        if( pos == std::wstring::npos )
        {
            continue;
        }
        else
        {
            strTypeFile = item.second;
            return true;
        }
    }
    strTypeFile = "";
    return false;
}

bool LWS::HttpSession::isResourceExist( const std::string& strResource )
{
    if( _access( strResource.c_str(), 0 ) != -1 )
    {
        return true;
    }
    return false;
}

bool LWS::HttpSession::writeFileInBody( const std::string& strFileName, std::stringstream& streamBody )
{
    std::ifstream fStream( strFileName, std::ifstream::in );
    if( fStream.is_open() )
    {
        streamBody << fStream.rdbuf();
        return true;
    }
    return false;
}

