#include "http_settings.h"
#include <io.h>

const UINT16 LWS::HttpSettings::s_sizeStrMax = 256;

LWS::HttpSettings& LWS::HttpSettings::instance()
{
    static HttpSettings instance;
    return instance;
}

void LWS::HttpSettings::setPort( const std::string& port )
{
    m_port = port;
}

const std::string& LWS::HttpSettings::getPortS() const
{
    return m_port;
}

void LWS::HttpSettings::setIp( const std::string& ip )
{
    m_ip = ip;
}

const std::string& LWS::HttpSettings::getIp() const
{
    return m_ip;
}

void LWS::HttpSettings::setDocumentRoot( const std::string& directoryRoot )
{
    m_documentRoot = directoryRoot;
}

const std::string& LWS::HttpSettings::getDocumentRoot() const
{
    return m_documentRoot;
}

void LWS::HttpSettings::addDataType( const std::string& type, const std::string& extensions )
{
    if( type.empty() || extensions.empty() )
        return;
    
    std::string extensionOne;
    for( size_t posOffset = 0, pos = 0; posOffset != std::string::npos; )
    {
        if( posOffset == 0 )
            pos = extensions.find( _T( ',' ), posOffset );
       else
           pos = extensions.find( _T( ',' ), ++posOffset );
       
        extensionOne = extensions.substr( posOffset, pos - posOffset );
        posOffset = pos;

        if( !isDataTypeExist( type, extensionOne ) )
            m_mmDataTypes.insert( std::make_pair( extensionOne, type ) );
    }
}

bool LWS::HttpSettings::readConfigFile( const std::string& strFileName )
{
    if( _access( strFileName.c_str(), 0 ) == -1 )
    {
        return false;
    }

    std::string result = LWS::HttpSettings::readConfigParam( "General", "Listen", strFileName );
    {
        int size = result.size();
        size_t pos = result.find( _T( ':' ) );
        if( pos == std::string::npos )
        {
            return false;
        }
        m_ip = result.substr( 0, pos );
        m_port = result.substr( pos + 1 );
    }

    m_documentRoot = LWS::HttpSettings::readConfigParam( "General", "DocumentRoot", strFileName );

    result = LWS::HttpSettings::readConfigParam( "AddType", "text/plain", strFileName );
    addDataType( "text/plain", result );
    result = LWS::HttpSettings::readConfigParam( "AddType", "text/html", strFileName );
    addDataType( "text/html", result );

    return true;
}

bool LWS::HttpSettings::isDataTypeExist( const std::string& type, const std::string& extension )
{
    auto result = m_mmDataTypes.equal_range( type );

    if( result.first == result.second )
        return false;

    for( auto it = result.first; it != result.second; ++it )
    {
        if( it->second == extension )
            return true;
    }
    return false;
}

size_t LWS::HttpSettings::dataTypeCount() const
{
    return m_mmDataTypes.size();
}

const std::map<std::string, std::string>& LWS::HttpSettings::getMultiMapDataType() const
{
    return m_mmDataTypes;
}

std::string LWS::HttpSettings::readConfigParam( const std::string& sectionName, const std::string& paramName, const std::string& fileName )
{
    int symbCount = 0;
    std::string param( s_sizeStrMax, _T( '\0' ) );
    symbCount = GetPrivateProfileString( sectionName.c_str(), paramName.c_str(), NULL, &param[0], param.size(), fileName.c_str() );
    param.resize( symbCount );
    return std::string( param );
}
