#pragma once
//#include "ithread_pool_work.hpp"
#include <WinSock2.h>

#include <string>

#include "http_settings.h"

namespace LWS
{
    class HttpSession /*:
        public IThreadPoolWork<HttpSession>*/
    {
    public:
        ~HttpSession();

        static void start( PTP_CALLBACK_INSTANCE instance, PVOID parameter, PTP_WORK work );
       
    protected:
        void setWorkData( PTP_CALLBACK_INSTANCE instance, PVOID parameter, PTP_WORK work ) /*override*/;
        void run() /*override*/;
 
    private:
        static const UINT16 s_sizeBuffer;
        SOCKET m_socket = NULL;

        int readRequest( std::string& strRequest );
        int writeResponse( const std::string& strResponse );
        bool checkResourceDataType( const std::string& strResource, std::string& strTypeFile );
        bool isResourceExist( const std::string& strNameResource );
        bool writeFileInBody( const std::string& strFileName, std::stringstream& streamBody );
    };
}
