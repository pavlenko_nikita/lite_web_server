#include "http_server.h"

#include <string>

#include "http_settings.h"
#include "http_session.h"

LWS::HttpServer::~HttpServer()
{
    stop();
    destroyThreadPool();
}

bool LWS::HttpServer::start()
{
    if( WSAStartup( MAKEWORD( 2, 2 ), &m_wsaData ) != 0 )
    {
        return false;
    }

    ADDRINFOA hints;
    ZeroMemory( &hints, sizeof( hints ) );

    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    if( GetAddrInfo(
        LWS::HttpSettings::instance().getIp().c_str(),
        LWS::HttpSettings::instance().getPortS().c_str(),
        &hints,
        &m_addr ) != 0 )
    {
        WSACleanup();
        return false;
    }

    m_socketServer = socket( m_addr->ai_family, m_addr->ai_socktype, m_addr->ai_protocol );
    if( m_socketServer == INVALID_SOCKET )
    {
        FreeAddrInfo( m_addr );
        WSACleanup();
        return false;
    }

    if( bind( m_socketServer, m_addr->ai_addr, static_cast<int>(m_addr->ai_addrlen) ) == SOCKET_ERROR )
    {
        FreeAddrInfo( m_addr );
        closesocket( m_socketServer );
        WSACleanup();
        return false;
    }

    if( listen( m_socketServer, SOMAXCONN ) == SOCKET_ERROR )
    {
        FreeAddrInfo( m_addr );
        closesocket( m_socketServer );
        WSACleanup();
        return false;
    }

    if( !initThreadPool() )
    {
        FreeAddrInfo( m_addr );
        closesocket( m_socketServer );
        WSACleanup();
        return false;
    }

    m_threadServer = std::thread( [this]()
    {
        runLoop();
    } );

    return true;
}

void LWS::HttpServer::stop()
{
    m_statusServer = EServerStatus::eES_Stop;
    WaitForThreadpoolWorkCallbacks( m_work, TRUE );
    if ( m_socketServer != INVALID_SOCKET)
        closesocket( m_socketServer );
    WSACleanup();
}

void LWS::HttpServer::joinLoop()
{
    if( m_threadServer.joinable() )
    {
        m_threadServer.join();
    }
}

void LWS::HttpServer::runLoop()
{
    SOCKET socketClient;
    m_statusServer = EServerStatus::eES_Running;
    while( m_statusServer == EServerStatus::eES_Running )
    {
        socketClient = accept( m_socketServer, NULL, NULL );
        if( socketClient != INVALID_SOCKET && m_statusServer == EServerStatus::eES_Running )
        {
            m_work = CreateThreadpoolWork( HttpSession::start, reinterpret_cast<PVOID>(socketClient), &m_callBackEnviron );
            if( m_work )
                SubmitThreadpoolWork( m_work );
        }
    }
}

bool LWS::HttpServer::initThreadPool()
{
    SYSTEM_INFO sysinfo;
    GetSystemInfo( &sysinfo );

    InitializeThreadpoolEnvironment( &m_callBackEnviron );
    m_pool = CreateThreadpool( NULL );
    SetThreadpoolThreadMaximum( m_pool, sysinfo.dwNumberOfProcessors );
    if( !SetThreadpoolThreadMinimum( m_pool, 1 ) )
    {
        return false;
    }
    m_cleanupGroup = CreateThreadpoolCleanupGroup();
    if( m_cleanupGroup == NULL )
    {
        return false;
    }
    SetThreadpoolCallbackPool( &m_callBackEnviron, m_pool );
    SetThreadpoolCallbackCleanupGroup( &m_callBackEnviron, m_cleanupGroup, NULL );

    return true;
}

void LWS::HttpServer::destroyThreadPool()
{
    CloseThreadpoolWork( m_work );
    CloseThreadpoolCleanupGroupMembers( m_cleanupGroup, TRUE, NULL );
    CloseThreadpoolCleanupGroup( m_cleanupGroup );
    CloseThreadpool( m_pool );
}
