﻿#include <iostream>
#include <sstream>
#include <string>

#include "http_server.h"
#include "http_settings.h"

int main( int argc, char* argv[] )
{
    if( argc == 1 )
        return 1;
    
    std::string path = argv[1];

    if( LWS::HttpSettings::instance().readConfigFile( path ) )
    {
        LWS::HttpServer server;
        if( server.start() )
            server.joinLoop();
    }

    return 0;
}

